package ch.admin.bit.mastermind.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Code {
    private List<Pin> code = new ArrayList<>();

    public Code(Code code) {
        this.code.addAll(code.getCode());
    }

    public Code() {
        this(4);
    }

    public Code(int length) {
        Random random = new Random();
        Color[] colors = Color.values();

        for (int i = 0; i < length; i++)
            code.add(new Pin(colors[random.nextInt(colors.length)]));
    }

    public Code(Pin... code) {
        Collections.addAll(this.code, code);
    }

    public Code(List<Pin> code) {
        this.code = code;
    }

    public List<Pin> getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code.toString();
    }
}
