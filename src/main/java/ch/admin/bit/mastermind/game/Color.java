package ch.admin.bit.mastermind.game;

public enum Color {
    RED, GREEN, BLUE, YELLOW, WHITE, BLACK
}
