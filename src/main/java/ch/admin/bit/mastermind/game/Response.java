package ch.admin.bit.mastermind.game;

public class Response {
    private int correctPins;
    private int correctColors;

    public Response(int correctPins, int correctColors) {
        this.correctPins = correctPins;
        this.correctColors = correctColors;
    }

    public int getCorrectPins() {
        return correctPins;
    }

    public int getCorrectColors() {
        return correctColors;
    }

    @Override
    public String toString() {
        return String.format("Korrekte Pins: %d; Korrekte Farben: %d", correctPins, correctColors);
    }
}
