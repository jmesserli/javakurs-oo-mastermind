package ch.admin.bit.mastermind.game;

public class Pin {
    private Color color;

    public Pin(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pin)) return false;

        Pin pin = (Pin) o;

        return getColor() == pin.getColor();
    }

    @Override
    public int hashCode() {
        return getColor() != null ? getColor().hashCode() : 0;
    }

    @Override
    public String toString() {
        return color.toString();
    }
}
