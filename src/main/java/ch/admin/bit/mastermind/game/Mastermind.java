package ch.admin.bit.mastermind.game;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Mastermind {
    private Code code;
    private Code tempCode;
    private int tries;

    public Mastermind(int length, int tries) {
        this.code = new Code(length);
        this.tries = tries;
    }

    public Mastermind(Code code, int tries) {
        this.code = code;
        this.tries = tries;
    }

    public Response checkCode(Code code) {
        tries--;
        return new Response(perfectMatches(code), colorMatches(code));
    }

    private int perfectMatches(Code entered) {
        int matches = 0;
        tempCode = new Code(code);

        for (int i = 0; i < entered.getCode().size(); i++) {
            Pin correctPin = tempCode.getCode().get(i);
            Pin enteredPin = entered.getCode().get(i);

            if (correctPin.equals(enteredPin)) {
                matches++;
                tempCode.getCode().set(i, null);
                entered.getCode().set(i, null);
            }
        }

        tempCode = cleanCode(tempCode);
        entered = cleanCode(entered);

        return matches;
    }

    private int colorMatches(Code entered) {
        int matches = 0;

        for (Pin pin : entered.getCode()) {
            if (tempCode.getCode().contains(pin)) {
                tempCode.getCode().remove(pin);
                matches++;
            }
        }

        return matches;
    }

    private static Code cleanCode(Code code) {
        return new Code(code.getCode().stream().filter(Objects::nonNull).collect(Collectors.toList()));
    }

    public void startGame() {
        Scanner in = new Scanner(System.in);

        System.out.println("Mastermind!\n");

        while (tries > 0) {
            boolean validTry = false;
            Code currentCode;

            System.out.println("------------------------------------------------");
            System.out.printf("Noch %d Versuche übrig.%n", tries);
            System.out.println();

            ArrayList<Color> colors = new ArrayList<>();
            while (!validTry) {
                colors.clear();
                System.out.print("Dein Versuch: ");
                String userTry = in.nextLine();
                if (userTry.length() != code.getCode().size()) {
                    System.out.println("Ungültig, versuchs nochmals.");
                    continue;
                }

                for (char colorChar : userTry.toCharArray()) {
                    Color color = null;

                    switch (colorChar) {
                        case 'r':
                            color = Color.RED;
                            break;
                        case 'g':
                            color = Color.GREEN;
                            break;
                        case 'b':
                            color = Color.BLUE;
                            break;
                        case 'y':
                            color = Color.YELLOW;
                            break;
                        case 'w':
                            color = Color.WHITE;
                            break;
                        case 'B':
                            color = Color.BLACK;
                            break;
                        default:
                            System.out.printf("Ungültige Farbe '%s'. Versuchs nochmals.%n", colorChar);
                    }

                    if (color != null)
                        colors.add(color);
                }

                if (colors.size() == code.getCode().size())
                    validTry = true;
            }

            currentCode = new Code(colors.stream().map(Pin::new).collect(Collectors.toList()));
            Response response = checkCode(currentCode);

            System.out.println(response);
            if (response.getCorrectPins() == code.getCode().size()) {
                System.out.println("Gewonnen! Der Code ist korrekt.");
                return;
            }
        }

        System.out.println("Verloren! Der Code war: " + code);
    }
}
