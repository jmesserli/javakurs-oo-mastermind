package ch.admin.bit.mastermind;

import ch.admin.bit.mastermind.game.Mastermind;

public class App {
    public static void main(String[] args) {
        Mastermind mastermind = new Mastermind(4, 4);
        mastermind.startGame();
    }
}
